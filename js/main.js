let mainArr = [], iList = 0;

function check(e){
    const form = new FormData(e.target);
    const name = form.get("name");
    const sname = form.get("sname");
    insertArray(name, sname);
    return false;
};

function insertArray(name, sname) {
    mainArr.push([{name: `${name}`, sname: `${sname}`}]);
    for (let i = 0; i < mainArr.length; i++) {
        sendUI(mainArr[i][0], iList);
        iList++;
    }
    mainArr = [];
}

function sendUI(obj, iList) {
    let main = document.querySelector('main');

    let elem = document.createElement("div");
    let name1 = document.createElement("h1");
    let name2 = document.createElement("h1");
    let del = document.createElement("input");

    name1.innerText = `${obj.name} (${iList})`;
    name2.innerText = `${obj.sname} (${iList})`;

    elem.setAttribute("class", "object-main");
    elem.setAttribute("id", `object-main-${iList}`);
    name1.setAttribute("class", "object-name");
    name2.setAttribute("class", "object-sname");
    del.setAttribute("class", "object-sname");
    del.setAttribute("type", "submit");
    del.setAttribute("onclick", `delObj(${iList})`);

    main.appendChild(elem);
    elem.appendChild(name1);
    elem.appendChild(name2);
    elem.appendChild(del);
}

function delObj(i) {
    let obj = document.getElementById(`object-main-${i}`);
    obj.remove();
}

